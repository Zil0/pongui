#include "Player.hpp"
#include "Ball.hpp"
#include "Bonus.hpp"
#include "config.h"
#include "jeu.hpp"

Player::Player(uint8_t button_pin, CRGB color, uint16_t position, int8_t direction, Player *opponent)
    : button_pin(button_pin), color(color), position(position), direction(direction), opponent(opponent),
      first_hit_position(position + direction * (HITTING_RANGE - 1)) {}

RedPlayer::RedPlayer() : Player(RED_BUTTON_PIN, CRGB::Red, 0, 1, &game.players[1]) {}
BluePlayer::BluePlayer()
    : Player(BLUE_BUTTON_PIN, CRGB::Blue, EFFECTIVE_NUM_LEDS - 1, -1, &game.players[0]) {}

void Player::init() {
    this->life_points = initial_life_points;
}

bool Player::holding() {
    return digitalRead(button_pin) == LOW;
}

void Player::check_button_press() {
    pressing = check_for_pressing();
}

bool Player::check_for_pressing() {
    if(is_a_bot)
        return true;

    byte value = digitalRead(button_pin);

    // Ensure longpressing accounts only for one press
    if(button_state == value)
        return false;

    button_state = value;
    return value == LOW;
}

bool Player::send_back(Ball *ball) {
    uint16_t min_position = direction == 1 ? position : first_hit_position;
    uint16_t max_position = direction == 1 ? first_hit_position : position;
    if(!ball->is_in_range(min_position, max_position)) {
        if(pressing && !is_a_bot &&
           ball->is_in_range(min_position - hitting_range, max_position + hitting_range))
            ball->disable_send_back = true;
        return false;
    }

    if(bonus && bonus->auto_send_back(ball))
        return true;

    if(ball->disable_send_back)
        return false;

    if(!pressing)
        return false;

    if(is_a_bot)
        ball->set_position(random(min_position, max_position + 1));

    if(ball->get_position() == first_hit_position)
        ball->speed_boost = ball->first_hit_boost;
    else if(ball->get_position() == position)
        ball->speed_boost = ball->perfect_hit_boost;
    else if(direction == 1 && ball->get_position() < 3)
        ball->speed_boost = ball->late_hit_boost;
    else if(direction == -1 && ball->get_position() > position - 3)
        ball->speed_boost = ball->late_hit_boost;
    else
        ball->speed_boost = 1;

    return true;
}

CRGB Player::get_dot_color() {
    return bonus ? bonus->get_color() : color;
}
