#include "Game.hpp"
#include "jeu.hpp"

LifeAddBonus life_add_bonus = LifeAddBonus();
SpeedingBonus speeding_bonus = SpeedingBonus();
RainbowBonus rainbow_bonus = RainbowBonus();
BlinkingBonus blinking_bonus = BlinkingBonus();
BreakoutBonus breakout_bonus = BreakoutBonus();
BallAddBonus ball_add_bonus = BallAddBonus();
PortalBonus portal_bonus = PortalBonus();

void Game::init() {
    randomSeed(millis());
    autoplay_timer = 0;
    players[0].init();
    players[1].init();
    start_new_round(sender, true);
}

bool Game::should_start() {
    if(!check_buttons())
        // User released the button before starting autoplay mode
        return autoplay_timer != 0;
    if(!autoplay_timer)
        autoplay_timer = millis();
    else if(millis() - autoplay_timer > 4000) {
        receiver->is_a_bot = true;
        return true;
    }
    return false;
}

bool Game::check_buttons() {
    for(byte i = 0; i < 2; i++) {
        if(players[i].holding()) {
            sender = &players[i];
            receiver = &players[(i + 1) % 2];
            return true;
        }
    }
    return false;
}

void Game::play() {
    switch(state) {
    case GETTING_READY:
        if(!strip.animate()) {
            scheduled_bonus_micros = micros() + get_bonus_delay_ns();
            add_ball(sender);
            state = PLAYING;
        }
        break;
    case PLAYING:
        pingpong();
        break;
    case ENDING:
        if(!strip.animate())
            state = WAITING;
        break;
    default:
        return;
    }
}

void Game::pingpong() {
    current_micros = micros();

    strip.turn_all_off();

    for(uint8_t i = 0; i < 2; i++) {
        players[i].check_button_press();
    }

    display_hitting_zone_dot();

    first_ball_index = (first_ball_index + 1) % ball_total;
    for(uint8_t i = 0; i < ball_total; i++) {
        Ball *ball = &balls[(first_ball_index + i) % ball_total];
        if(!ball->active)
            continue;

        ball->display();

        if(ball->sender->pressing)
            check_for_bonus(ball);

        if(ball->receiver->send_back(ball)) {
            send_back(ball);
        }

        Player *hitted_player = ball->move();
        if(hitted_player) {
            remove_ball(ball);
            if(--hitted_player->life_points == 0)
                return end_game(hitted_player->opponent);
            else if(ball_count == 0)
                return start_new_round(hitted_player->opponent);
        }
    }

    add_scheduled_bonus();
    display_bonus();
}

void Game::send_back(Ball *ball) {
    if(ball->bonus)
        ball->remove_bonus();

    ball->switch_player();

    if(ball->sender->bonus)
        ball->use_bonus();

    if(ball->speed_boost == ball->first_hit_boost) // sneaky attack
        ball->set_position(round(strip.num_leds / 2));
}

void Game::remove_ball(Ball *ball) {
    ball->deactivate();
    ball_count--;
}

void Game::start_new_round(Player *sender, bool first_round) {
    for(short i = 0; i < bonuses_count; i++)
        bonuses[i]->state = NONE;

    for(short i = 0; i < 2; i++)
        players[i].bonus = nullptr;

    this->sender = sender;
    strip.turn_all_off();
    strip.set_animation(new RoundAnimation(sender, first_round));
    state = GETTING_READY;
}

Ball *Game::add_ball(Player *player) {
    for(uint8_t i = 0; i < ball_total; i++) {
        if(!balls[i].active) {
            ball_count++;
            return balls[i].activate(player);
        }
    }
    return nullptr;
}

void Game::end_game(Player *winner) {
    players[0].is_a_bot = players[1].is_a_bot = false;
    for(uint8_t i = 0; i < ball_total; i++)
        balls[i].deactivate();
    ball_count = 0;
    strip.turn_all_off();
    strip.set_animation(new WinAnimation(winner));
    state = ENDING;
}

void Game::display_hitting_zone_dot() {
    for(byte i = 0; i < 2; i++) {
        // Fill a bot hitting zone with its faded color
        if(players[i].is_a_bot) {
            CRGB color = players[i].get_dot_color();
            color = color.fadeToBlackBy(224);
            short start;
            short end;
            if(players[i].direction == 1) {
                start = players[i].position;
                end = players[i].first_hit_position;
            } else {
                start = players[i].first_hit_position;
                end = players[i].position;
            }
            strip.turn_range_on(color, start, end);
        }
        strip.turn_on(players[i].get_dot_color(), players[i].first_hit_position);
#if SHOW_LAST_HIT_POSITION
        strip.turn_on(players[i].get_dot_color(), players[i].position);
#endif
    }
}

void Game::display_bonus() {
    for(short i = 0; i < bonuses_count; i++)
        bonuses[i]->display();
}

uint32_t Game::get_bonus_delay_ns() {
    uint32_t min_bonus_delay = 1000000 * MIN_BONUS_DELAY;
    uint32_t max_bonus_delay = 1000000 * MAX_BONUS_DELAY;
    return min_bonus_delay + random(max_bonus_delay - min_bonus_delay);
}

void Game::add_scheduled_bonus() {
    if(current_micros < scheduled_bonus_micros)
        return;
    uint32_t total_proba = 0;
    for(uint8_t i = 0; i < bonuses_count; i++) {
        Bonus *bonus = bonuses[i];
        if(!bonus->can_be_added())
            continue;
        total_proba += bonus->probability;
    }
    uint32_t proba = random(total_proba);
    uint32_t incr_proba = 0;
    for(uint8_t i = 0; i < bonuses_count; i++) {
        Bonus *bonus = bonuses[i];
        if(!bonus->can_be_added())
            continue;
        incr_proba += bonus->probability;
        if(proba < incr_proba) {
            bonus->action_on_display();
            break;
        }
    }
    scheduled_bonus_micros = current_micros + get_bonus_delay_ns();
}

void Game::check_for_bonus(Ball *ball) {
    for(short i = 0; i < bonuses_count; i++) {
        Bonus *bonus = bonuses[i];
        if(bonus->can_be_taken(ball))
            bonus->action_on_take(ball);
    }
}
