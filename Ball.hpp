#ifndef BALL_H
#define BALL_H

#include "config.h"
#include <Arduino.h>
#include <FastLED.h>

class Bonus;
class Player;
class Strip;

class Ball {
  private:
    static constexpr byte length = BALL_LENGTH;
    short start_position;
    short position;

  public:
    static constexpr float perfect_hit_boost = PERFECT_HIT_BOOST;
    static constexpr float late_hit_boost = LATE_HIT_BOOST;
    static constexpr float first_hit_boost = FIRST_HIT_BOOST;
    Player *sender;
    Player *receiver;
    Bonus *bonus = nullptr;
    unsigned long previous_micros = 0;
    bool active = false;
    bool disable_send_back = false;
    float speed_boost = 1;
    bool is_in_range(short start, short end);
    void display();
    Ball *activate(Player *player);
    void deactivate();
    void use_bonus();
    void remove_bonus();
    void switch_player();
    float get_speed_boost();
    Player *move();
    short get_start_position();
    short get_position();
    void set_position(short position);
};

#endif
