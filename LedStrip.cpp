#include "LedStrip.hpp"

void Strip::setup() {
#ifdef LED_CLOCK_PIN
    FastLED.addLeds<LED_TYPE, LED_PIN, LED_CLOCK_PIN, COLOR_ORDER>(leds_buffer, NUM_LEDS);
#else
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds_buffer, NUM_LEDS);
#endif
    FastLED.setBrightness(BRIGHTNESS);

    // Turn all strip black, without taking care of offsets
    memset8((void *)leds_buffer, 0, sizeof(struct CRGB) * NUM_LEDS);
    FastLED.show();

    // Re-initialize strip with offsets
    FastLED[0].setLeds(leds_buffer, NUM_LEDS - END_OFFSET);
};

void Strip::turn_on(CRGB color, short position) {
#ifdef DEBUG
    if(position < 0 || position > num_leds - 1) {
        Serial.print(F("Turn on out of range: "));
        Serial.println(position);
        return;
    }
#endif
    leds[position] = color;
}

void Strip::turn_off(short position) {
    turn_on(CRGB::Black, position);
}

void Strip::turn_range_on(CRGB color, short start, short end, byte step) {
#ifdef DEBUG
    if(start > end || start < 0 || end > num_leds - 1) {
        Serial.print(F("Turn range on out of range: "));
        Serial.print(start);
        Serial.print(" ");
        Serial.print(end);
        Serial.print(" ");
        Serial.println(step);
        return;
    }
#endif
    for(uint16_t i = start; i <= end; i += step)
        turn_on(color, i);
}

void Strip::turn_range_gradient(CRGB startcolor, short startpos, CRGB endcolor, short endpos) {
#ifdef DEBUG
    if(startpos > endpos || startpos < 0 || endpos > num_leds - 1) {
        Serial.print(F("Turn range gradient out of range: "));
        Serial.print(startpos);
        Serial.print(" ");
        Serial.println(endpos);
        return;
    }
#endif
    fill_gradient_RGB(leds, startpos, startcolor, endpos, endcolor);
}

void Strip::turn_range_rainbow(short start, short end, uint8_t initialhue, uint8_t deltahue) {
#ifdef DEBUG
    if(start > end || start < 0 || end > num_leds - 1) {
        Serial.print(F("Rainbow out of range: "));
        Serial.print(start);
        Serial.print(" ");
        Serial.println(end);
        return;
    }
#endif
    CHSV hsv;
    hsv.hue = initialhue;
    hsv.val = 255;
    hsv.sat = 240;
    for(uint16_t i = start; i <= end; i += 1) {
        turn_on(hsv, i);
        hsv.hue += deltahue;
    }
}

void Strip::turn_range_off(short start, short end, byte step) {
    turn_range_on(CRGB::Black, start, end, step);
}

void Strip::turn_all_off() {
    memset8((void *)leds, 0, sizeof(struct CRGB) * num_leds);
}

void Strip::set_random_color_led(short position) {
    turn_on(get_random_color(), position);
}

CRGB Strip::get_random_color() {
    return CRGB(random(256), random(256), random(256));
}

void Strip::set_animation(Animation *anim) {
    if(animation)
        delete animation;
    animation = anim;
    animation->init();
}

bool Strip::animate() {
    if(animation) {
        if(animation->done()) {
            delete animation;
            animation = nullptr;
        } else {
            animation->play();
        }
    }
    return animation != nullptr;
}
