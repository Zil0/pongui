#include "Screensaver.hpp"
#include "Animation.hpp"
#include "LedStrip.hpp"
#include "jeu.hpp"

void Screensaver::start() {
    Animation *animation;
    switch(random(3)) {
    case 0:
        Serial.println(F("Start screensaver 'Random Colors'"));
        animation = new RandomColors();
        break;
    case 1:
        Serial.println(F("Start screensaver 'Accumulating Dots'"));
        animation = new AccumulatingDots();
        break;
    case 2:
        Serial.println(F("Start screensaver 'Rasta Flag'"));
        animation = new RastaFlag();
        break;
    }
    strip.set_animation(animation);
}

void Screensaver::animate() {
    if(!strip.animate()) {
        start();
    }
}
