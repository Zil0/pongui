#ifndef CONFIG_H
#define CONFIG_H

#include "default_config.h"

#if __has_include("config_override.h")
#include "config_override.h"
#endif

#define EFFECTIVE_NUM_LEDS (NUM_LEDS - START_OFFSET - END_OFFSET)

#endif
