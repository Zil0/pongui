#include "Bonus.hpp"
#include "Game.hpp"
#include "LedStrip.hpp"
#include "Screensaver.hpp"
#include <FastLED.h>

#include "config.h"

Strip strip = Strip();
Screensaver screensaver = Screensaver();
Game game = Game();

void setup() {
    Serial.begin(BAUD_RATE);
    Serial.println(F("Hello, world!"));

    pinMode(RED_BUTTON_PIN, INPUT_PULLUP);
    pinMode(BLUE_BUTTON_PIN, INPUT_PULLUP);

    strip.setup();
}

void loop() {
    FastLED.show();

    if(game.state != WAITING)
        game.play();
    else if(game.should_start())
        game.init();
    else
        screensaver.animate();
}
