#ifndef PLAYER_H
#define PLAYER_H

#include "config.h"
#include <Arduino.h>
#include <FastLED.h>

class Bonus;
class Ball;

class Player {
  private:
    byte button_pin;
    byte button_state;
    bool check_for_pressing();

  public:
    Player(uint8_t button_pin, CRGB color, uint16_t position, int8_t direction, Player *opponent);
    static const byte hitting_range = HITTING_RANGE;
    static const byte initial_life_points = POINTS_TO_WIN;
    const CRGB color;
    const uint16_t first_hit_position;
    const int8_t direction;
    uint16_t position;
    byte life_points;
    bool is_a_bot = false;
    Bonus *bonus;
    Player *opponent;
    void check_button_press();
    bool pressing = false;
    void init();
    bool holding();
    bool send_back(Ball *ball);
    void score();
    CRGB get_dot_color();
};

class RedPlayer : public Player {
  public:
    RedPlayer();
};

class BluePlayer : public Player {
  public:
    BluePlayer();
};

#endif
