#ifndef GAME_H
#define GAME_H

#include "Animation.hpp"
#include "Ball.hpp"
#include "Bonus.hpp"
#include "LedStrip.hpp"
#include "Player.hpp"
#include "config.h"
#include <Arduino.h>
#include <FastLED.h>

class Player;
class Strip;
class Ball;
class Bonus;

extern LifeAddBonus life_add_bonus;
extern SpeedingBonus speeding_bonus;
extern RainbowBonus rainbow_bonus;
extern BlinkingBonus blinking_bonus;
extern BreakoutBonus breakout_bonus;
extern BallAddBonus ball_add_bonus;
extern PortalBonus portal_bonus;

enum GameState {
    WAITING = 0,
    GETTING_READY,
    PLAYING,
    ENDING,
};

class Game {
  private:
    Player *sender;
    Player *receiver;
    unsigned long scheduled_bonus_micros;
    Ball balls[3] = {Ball(), Ball(), Ball()};
    unsigned long autoplay_timer = 0;
    uint8_t first_ball_index = 0;
    void pingpong();
    void switch_player();
    void display_ball();
    void display_hitting_zone_dot();
    void display_bonus();
    uint32_t get_bonus_delay_ns();
    void add_scheduled_bonus();
    void check_for_bonus(Ball *ball);
    bool check_buttons();

  public:
    GameState state = WAITING;
    static constexpr unsigned long speed = DELAY;
    static constexpr short bonuses_count = 7;
    Bonus *bonuses[bonuses_count] = {
        &life_add_bonus, &speeding_bonus, &rainbow_bonus, &blinking_bonus,
        &breakout_bonus, &ball_add_bonus, &portal_bonus,
    };
    Player players[2] = {RedPlayer(), BluePlayer()};
    static constexpr unsigned long ball_total = 3;
    uint8_t ball_count = 0;
    unsigned long current_micros;
    void play();
    bool should_start();
    void init();
    void start_new_round(Player *sender, bool first_round = false);
    void end_game(Player *player);
    Ball *add_ball(Player *player);
    void remove_ball(Ball *ball);
    void send_back(Ball *ball);
};

#endif
